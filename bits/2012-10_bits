Hi,

in this bits:
  1. Scientific citations
  2. Prospective packages in VCS
  3. Proposal to move Blends code from SVN to Git
  4. General situation of Blends

1. Scientific citations
-----------------------

Due to the effort of Charles Plessy to gather machine readable
information about upstream[1a] there is a set of references about
scientific publications available.  This is an ideal situation for
scientific Blends like Debian Science, Debian Med and DebiChem because
it opens the chance to strengthen the connection to upstream authors of
software by advertising their scientific work.  While the web sentinel
provides a way to inject such citations directly inside the tasks files
of a Blend this had several drawbacks which are now solved:

  1. Citations do not need to be specified in every single task
     file in case a package is useful in more than one task.
  2. The reference information is directly attached to a package
     and can be used there for other purposes as well.
  3. The infromation of debian/upstream files is parsed in Vcs
     and injected in UDD[1b] were it can be used quite flexible.

Technically the Upstream Metadata Gatherer[1a] parses the packaging
VCSes once a day and creates a tar archive which in turn is parsed by
the UDD[1b] bibref gather.  Once the data are inside UDD there are
several chances to use these also independently from the usage inside
Blends sentinal pages.  For instance you can create a BibTeX file of all
citations inside Debian which was explained at Debian Science mailing
list[1c].

[1a] https://wiki.debian.org/UpstreamMetadata
[1b] https://wiki.debian.org/UltimateDebianDatabase
[1c] https://lists.debian.org/debian-science/2012/04/msg00077.html
     (and other mails in this mailing list)


2. Prospective Packages in VCS
------------------------------

The Blends tasks files may contain so called prospective packages[2a].
On one hand these entries are informing users what software the
packaging team is working on and on the other hand these entries ensure
that metapackages are built nicely without changes once the package in
question has reached the Debian mirror.  It has turned out that some
tasks files became quite long because of these prospective packages
entries and on the other hand information of packages that are just
prepared in VCS of a packaging team is duplicated.

To enhance this situation a parser was written that gathers information
of all machine readable files in VCS of the Blends packaging teams and
injects this information into an UDD[1b] table.  From there it is read
when the tasks pages of the web sentinel are rendered and thus the extra
information about prospective packages in the tasks file becomes
redundant.  The raw archive of this information can be found here[2b].

[2a] http://blends.alioth.debian.org/blends/ch-sentinel.en.html#s-edittasksfiles
[2b] http://blends.debian.net/packages-metadata/packages-metadata.tar.bz2


3. Proposal to move Blends code from SVN to Git
-----------------------------------------------

A proposal was posted on the Blends listi[3a] to switch the repository
maintaining Blends code from SVN to Git.  If you are involved in
maintaining parts of the code and are not happy with this move this is
your chance to raise your voice now because there are chances to leave
some parts which you might be interested in untouched if your personal
workflow would be spoiled by this move.

[3a] https://lists.debian.org/debian-blends/2012/10/msg00002.html


4. General situation of Blends
------------------------------

Debian Accessibility
    Team works constantly and successful to enhance accessibility
    features of Debian

BrDesktop
    Became a bit silent in the last time - some bits of the drivers
    might be interesting.

DebiChem
    Team works constantly on maintenance of existing and new
    packages which are useful for Chemists.

Debian Edu
    Has released Debian Edu based on Squeeze and made good progress
    to gather new team members.

Debian EzGo
    Became a bit silent in the last time - some bits of the drivers
    might be interesting.

Debian GIS
    Team works constantly on maintenance of existing and new
    packages used in Geographic Information Systems as well as
    OpenStreetMap.  It might not harm if the team would be more
    visible in and outside Debian because there should be
    potentially a lot of more users and developers.

Debian Junior
    Recently some new light might be at horizon to revitalise
    the project - at least the mailing list[4a] recived some
    promising mails.

Debian Lex
    Did not really started ever, recently there was some activity
    on the mailing list to vitalise the project a bit.  On the
    other hand it was suggested by Debian web team to state on
    Debian Lex website that the project is discontinued.

Debian Med
    Constant progress to package even more software for medical
    care and biological research as well as a constant increase
    of developers in the team.  The progress was boosted by
    the yearly sprints (see last Debian Med bits in March[4b])

Debian Multimedia
    Team works constantly on maintenance of existing and new
    multimedia packages.  It might not harm if the team would be
    more visible in and outside Debian because there should be
    potentially a lot of more users and developers.

Debian Science
    Constant progress to package even more scientific software
    which is not yet covered by Debian Med or DebiChem team.
    Also the number of tasks is increased.  It might make sense
    to branch some Debian Mathematics or Debian Physics project
    from Debian Science to add more stucture to those topics
    and create more dedicated teams.

In summary I would like to say that I do see good chances for
improving some Blends as well as using the technique in other
fields as well to make Debian more popular in additional
workfields.

[4a] http://lists.debian.org/debian-jr/
[4b] https://lists.debian.org/debian-devel-announce/2012/03/msg00009.html

Thanks for your interest in Debian Pure Blends and feel free
to read more about it[0]

       Andreas.

[0] http://blends.alioth.debian.org/blends/
